Feature: Belly

  Scenario: a few cukes
    Given I have <no_cakes> cukes in my belly
    When I wait <no_Hour> hour
    Then my belly should growl

    Examples:
    | no_cakes | no_Hour |
    |    450   |  26500  |
    |    500   |  29500  |
    |    575   |  31500  |
    |    600   |  37000  |